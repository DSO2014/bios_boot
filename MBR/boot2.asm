        mov ax, 0x7c0
        mov ds, ax
        
        mov bx, mensaje
print:  mov al,[ds:bx]
        cmp al,0      ;zero=end of str
        je fin        ;done!
        mov ah,0xe
        int 0x10
        inc bx
        jmp print

fin:    
		xor ax,ax
	    int 0x16
        jmp 0xf000:0xfff0 ; jump to FFFF0h (reset)

        times 32 db 0
        
mensaje db 'Pulsa una tecla para saltar a la direccion fisica FFFF0h!',10,13,0

        times 440-($-$$) db 0  ; relleno
        
        db 0,0,0,0,0,0,0x80,0x21  ; tabla de particiones
        db 0x03,0,0x83,0x2a,0xcc,0xf9,0,0x08  
        db 0,0,0,0x50,0x77,0,0,0
                
        times 510-($-$$) db 0 ; relleno
        db 0x55, 0xAA  ; marca MBR
