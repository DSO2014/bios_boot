#!/bin/bash

# Tiene dos argumentos ambos de entrada y salida:
#	$1 boot code file
#	$2 destination (file or /dev/...)
# 		usar con EXTREMA PRECAUCION sobre /dev/...!!!!!!!

#------------------------------------------------------------------------------
# leo el detino (solo 512 bytes), para salvar la tabla de particion
dd if=$2 of=PT count=1
split -b 440 PT
# me quedo con la segunda parte y elimino la primera
rm PT xaa

#------------------------------------------------------------------------------
split -b 440 $1 $1
# me quedo con los primeros 440 bytes, la segunda parte la elimino
rm $1ab

#------------------------------------------------------------------------------
#creo un BOOT final con la primera parte del primero y los p_entry
cat $1aa xab > $1
#elimino los ficheros intermedios
rm $1aa xab

#------------------------------------------------------------------------------
#coloco el boot code final en el destino
dd if=$1 of=$2 count=1


