; nasm -f elf test.s
; ld test.o -o test

global _start                ; Program Entry Point (linker)

section .text
align 4
      dd 0x1BADB002          ; MAGIC NUMBER
      dd 0x3                 ; FLAGS
      dd -(0x1BADB002+0x3)   ; CHECKSUM

_start:
      mov esp, stack+0x4000  ; set up the stack
      mov eax, 0xb8000
out:  mov byte [eax], 0x40
      inc eax
      mov byte [eax], al
      inc eax
      cmp eax, 0xb8000+80*25*2
      jne out

      cli
hang: hlt                    ; halt machine
      jmp   hang
 
section .bss
align 4
stack:  resb 0x4000          ; reserva 16k de pila

